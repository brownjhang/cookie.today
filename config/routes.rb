Rails.application.routes.draw do
  namespace :cms do
    resources :slips
  end

  get 'slips/random', :to => 'slips#random_show'

  root :to => 'root#index'
end
