json.extract! cms_slip, :id, :content, :created_at, :updated_at
json.url cms_slip_url(cms_slip, format: :json)
