class SlipsController < ApplicationController

  # GET /slips/random
  def random_show
    slip = Slip.rand_slip
    render json: {content: slip.content}
  end

end
