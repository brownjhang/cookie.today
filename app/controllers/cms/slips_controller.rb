class Cms::SlipsController < CmsController
  before_action :set_cms_slip, only: [:show, :edit, :update, :destroy]

  # GET /cms/slips
  # GET /cms/slips.json
  def index
    @cms_slips = Cms::Slip.sort_by_created_at_desc
  end

  # GET /cms/slips/1
  # GET /cms/slips/1.json
  def show
  end

  # GET /cms/slips/new
  def new
    @cms_slip = Cms::Slip.new
  end

  # GET /cms/slips/1/edit
  def edit
  end

  # POST /cms/slips
  # POST /cms/slips.json
  def create
    @cms_slip = Cms::Slip.new(cms_slip_params)

    respond_to do |format|
      if @cms_slip.save
        format.html { redirect_to @cms_slip, notice: 'Slip was successfully created.' }
        format.json { render :show, status: :created, location: @cms_slip }
      else
        format.html { render :new }
        format.json { render json: @cms_slip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cms/slips/1
  # PATCH/PUT /cms/slips/1.json
  def update
    respond_to do |format|
      if @cms_slip.update(cms_slip_params)
        format.html { redirect_to @cms_slip, notice: 'Slip was successfully updated.' }
        format.json { render :show, status: :ok, location: @cms_slip }
      else
        format.html { render :edit }
        format.json { render json: @cms_slip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cms/slips/1
  # DELETE /cms/slips/1.json
  def destroy
    @cms_slip.destroy
    respond_to do |format|
      format.html { redirect_to cms_slips_url, notice: 'Slip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cms_slip
      @cms_slip = Cms::Slip.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cms_slip_params
      params.require(:cms_slip).permit(:content)
    end
end
