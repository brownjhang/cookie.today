class CmsController < ActionController::Base
  layout "cms"
  protect_from_forgery with: :exception
end
