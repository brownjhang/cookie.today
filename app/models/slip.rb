class Slip < ApplicationRecord
  scope :rand_slip, -> { find(self.pluck(:id).sample) }
end
