class Cms::Slip < ApplicationRecord
  validates :content, :presence => true

  scope :sort_by_created_at_desc, -> { order(created_at: :desc) }
end
