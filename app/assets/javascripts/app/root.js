Root = {
    init: function(){
        $("#btn_open_cookie").on("click", function(e){
            e.preventDefault();
            $.ajax({
                type: 'GET',
                url: '/slips/random',
                processData: false,
                dataType: "json",
                success: function(resp){
                    $("#slip_content").text(resp.content).show();
                },
                error: function(){
                    alert("Unexpected Error!! Please contact us...")
                }
            });
        });
    }
}