class CreateSlips < ActiveRecord::Migration[5.1]
  def change
    create_table :slips do |t|
      t.string :content

      t.timestamps
    end
  end
end
