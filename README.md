# Cookie Today
Simple Fortune Cookie

Ruby Ver: 2.5.1
Rails Ver: 5.1.7

## Home Page
> GET /
>
> You can click the button on the page to draw a random slip.

## Management
> GET /cms/slips
>
> You can add/edit/destroy slips of this application.

## Test
```
$> rake spec
```

## Run on localhost
```
$> rails s [-p [port]]
```
