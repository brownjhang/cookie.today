require 'rails_helper'

RSpec.describe "cms/slips/index", type: :view do
  before(:each) do
    assign(:cms_slips, [
      Cms::Slip.create!(
        :content => "Content"
      ),
      Cms::Slip.create!(
        :content => "Content"
      )
    ])
  end

  it "renders a list of cms/slips" do
    render
    assert_select "tr>td", :text => "Content".to_s, :count => 2
  end
end
