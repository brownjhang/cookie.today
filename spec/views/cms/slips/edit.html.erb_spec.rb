require 'rails_helper'

RSpec.describe "cms/slips/edit", type: :view do
  before(:each) do
    @cms_slip = assign(:cms_slip, Cms::Slip.create!(
      :content => "MyString"
    ))
  end

  it "renders the edit cms_slip form" do
    render

    assert_select "form[action=?][method=?]", cms_slip_path(@cms_slip), "post" do

      assert_select "textarea[name=?]", "cms_slip[content]"
    end
  end
end
