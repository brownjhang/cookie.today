require 'rails_helper'

RSpec.describe "cms/slips/show", type: :view do
  before(:each) do
    @cms_slip = assign(:cms_slip, Cms::Slip.create!(
      :content => "Content"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Content/)
  end
end
