require 'rails_helper'

RSpec.describe "cms/slips/new", type: :view do
  before(:each) do
    assign(:cms_slip, Cms::Slip.new(
      :content => "MyString"
    ))
  end

  it "renders new cms_slip form" do
    render

    assert_select "form[action=?][method=?]", cms_slips_path, "post" do

      assert_select "textarea[name=?]", "cms_slip[content]"
    end
  end
end
