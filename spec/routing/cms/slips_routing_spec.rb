require "rails_helper"

RSpec.describe Cms::SlipsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/cms/slips").to route_to("cms/slips#index")
    end

    it "routes to #new" do
      expect(:get => "/cms/slips/new").to route_to("cms/slips#new")
    end

    it "routes to #show" do
      expect(:get => "/cms/slips/1").to route_to("cms/slips#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cms/slips/1/edit").to route_to("cms/slips#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/cms/slips").to route_to("cms/slips#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/cms/slips/1").to route_to("cms/slips#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/cms/slips/1").to route_to("cms/slips#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cms/slips/1").to route_to("cms/slips#destroy", :id => "1")
    end
  end
end
